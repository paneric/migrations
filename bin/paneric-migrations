#!/usr/bin/env php
<?php

declare(strict_types=1);

define('ROOT_FOLDER', dirname(__DIR__) . '/');

use DI\ContainerBuilder;
use Dotenv\Dotenv;
use Paneric\Migrations\Command\GenerateCommand;
use Paneric\Migrations\Command\MigrateCommand;
use Symfony\Component\Console\Application;
use Symfony\Component\Console\CommandLoader\FactoryCommandLoader;

require './vendor/autoload.php';

$dotEnv = Dotenv::createImmutable('./');
$dotEnv->load();

$rootFolder = dirname(__DIR__) . '/';

try {
    $builder = new ContainerBuilder();
    $builder->useAutowiring(true);
    $builder->useAnnotations(false);
    $builder->addDefinitions(array_merge(
        require 'src/config/dependencies/packages.php',
        require 'src/config/settings/console.php',
    ));

    $container = $builder->build();

    $commandLoader = new FactoryCommandLoader([
        'generate' => static function () use ($container) {
            return $container->get(GenerateCommand::class);
        },
        'migrate' => static function () use ($container) {
            return $container->get(MigrateCommand::class);
        },
    ]);

    $application = new Application();
    $application->setCommandLoader($commandLoader);
    $application->run();
} catch (Exception $e) {
    echo(sprintf(
        "%s%s%s%s",
        $e->getFile() . "\n",
        $e->getLine() . "\n",
        $e->getMessage() . "\n",
        $e->getTraceAsString() . "\n"
    ));
}
