CREATE TABLE IF NOT EXISTS `migration` (
    `mig_id` int(11) UNSIGNED NOT NULL,
    `mig_ref` varchar(255) COLLATE utf8_unicode_ci,
    `mig_description` varchar(255) COLLATE utf8_unicode_ci,
    `mig_created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `mig_updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
ALTER TABLE `migration`
    ADD PRIMARY KEY (`mig_id`),
    ADD UNIQUE KEY `mig_ref` (`mig_ref`),
    ADD KEY `mig_created_at` (`mig_created_at`),
    ADD KEY `mig_updated_at` (`mig_updated_at`),
    MODIFY `mig_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
