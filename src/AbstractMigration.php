<?php

declare(strict_types=1);

namespace Paneric\Migrations;

use Exception;

abstract class AbstractMigration
{
    public function __construct(readonly protected MigrationRepositoryInterface $migrationRepository)
    {
    }

    abstract public function getDescription(): string;

    /**
     * @throws Exception
     */
    protected function executeQuery(string $query): void
    {
        $ref = get_class($this);
        $entry = $this->migrationRepository->findOneByRef($ref);
        if ($entry !== null) {
            throw new Exception(sprintf(
                'Migration entry "%s" already exists.',
                $ref
            ));
        }
        
        $this->migrationRepository->execute(
            $query,
            [
                'ref' => $ref,
                'description' => $this->getDescription()                
            ]
        );
    }
}
