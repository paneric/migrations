<?php

declare(strict_types=1);

namespace Paneric\Migrations\Command;

class Config
{
    public function __construct(private readonly array $values) {
    }

    public function __invoke(): array
    {
        return $this->values;
    }
}
