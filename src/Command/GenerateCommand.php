<?php

declare(strict_types=1);

namespace Paneric\Migrations\Command;

use Exception;
use JetBrains\PhpStorm\NoReturn;
use Monolog\Handler\StreamHandler;
use Monolog\Level;
use Monolog\Logger;
use Paneric\Migrations\Handler\ConsoleOutputTrait;
use Paneric\Migrations\Handler\GenerateHandler;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(
    name: 'generate',
    description: 'Generates empty migration.'
)]
class GenerateCommand extends Command
{
    use ConsoleOutputTrait;
    
    private LoggerInterface $logger;

    public function __construct(private readonly GenerateHandler $generateHandler)
    {
        $this->logger = new Logger('migrations');
        $this->logger->pushHandler(new StreamHandler('var/log/migrations.log', Level::Debug));

        parent::__construct();
    }

    protected function configure(): void
    {
        $this
            ->addArgument(
                'path',
                InputArgument::REQUIRED,
                'Relative path to migrations classes folder.'
            )
            ->addOption(
                'suffix',
                '-s',
                InputOption::VALUE_OPTIONAL,
                'Suffix for a generated migration class name.',
                ''
            )
            ->setHelp(
                'This command allows you to generate an empty migration.'
            )
        ;
    }

    /**
     * @throws Exception
     */
    #[NoReturn]
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->setOutputFormatterStyles($output);
        
        try {
            $this->generateHandler->handle(
                $input->getArgument('path'),
                $input->getOption('suffix'),
                $output
            );

            return Command::SUCCESS;
        } catch (Exception $e) {
            $message = $e->getMessage();
            $this->logger->error($message);
            $this->notifyMigrationError($output, $message);
        }
        return Command::FAILURE;
    }
}
