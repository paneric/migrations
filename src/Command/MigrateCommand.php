<?php

declare(strict_types=1);

namespace Paneric\Migrations\Command;

use Exception;
use Monolog\Handler\StreamHandler;
use Monolog\Level;
use Monolog\Logger;
use Paneric\Migrations\Handler\ConsoleOutputTrait;
use Paneric\Migrations\Handler\MigrateHandler;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(
    name: 'migrate',
    description: 'Migrates migrations.'
)]
class MigrateCommand extends Command
{
    use ConsoleOutputTrait;
    
    private LoggerInterface $logger;

    public function __construct(private readonly MigrateHandler $migrateHandler)
    {
        $this->logger = new Logger('migrations');
        $this->logger->pushHandler(new StreamHandler('var/log/migrations.log', Level::Debug));

        parent::__construct();
    }

    protected function configure(): void
    {
        $this
            ->addOption(
                'file',
                '-f',
                InputOption::VALUE_OPTIONAL,
                'Migration class name to migrate.',
                ''
            )
            ->addOption(
                'mode',
                '-m',
                InputOption::VALUE_REQUIRED,
                'Migration process mode (up, upto, down, downto).',
                'up'
            )
            ->setHelp(
                'This command allows you to migrate migrations.'
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->setOutputFormatterStyles($output);
        
        try {
            $this->migrateHandler->handle(
                $input->getOption('file'),
                $input->getOption('mode'),
                $output
            );

            return Command::SUCCESS;
        } catch (Exception $e) {
            $message = $e->getMessage();
            $this->logger->error($message);
            $this->notifyMigrationError($output, $message);
        }
        return Command::FAILURE;
    }
}
