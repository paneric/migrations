<?php

declare(strict_types=1);

namespace Paneric\Migrations\Handler;

use Symfony\Component\Console\Formatter\OutputFormatterStyle;
use Symfony\Component\Console\Output\OutputInterface;

trait ConsoleOutputTrait
{
    private function setOutputFormatterStyles(OutputInterface $output): void
    {
        $output->getFormatter()->setStyle(
            'comment',
            new OutputFormatterStyle('yellow', null)
        );

        $output->getFormatter()->setStyle(
            'error',
            new OutputFormatterStyle('red', null)
        );
    }

    private function notifyMigrationError(OutputInterface $output, string $message): void
    {
        $output->writeln(sprintf(
            '<error>%s</error>',
            $message
        ));
    }

    private function notifyMigrationGeneration(OutputInterface $output, string $namespace): void
    {
        $output->writeln(sprintf(
            '<comment> Generated migration %s</comment>',
            $namespace
        ));
    }

    private function notifyMigrationExecution(OutputInterface $output, string $namespace, string $mode = ''): void
    {
        $output->writeln(sprintf(
            '<comment> Migrating %s %s</comment>',
            $namespace,
            $mode
        ));
    }
}
