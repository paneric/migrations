<?php

declare(strict_types=1);

namespace Paneric\Migrations\Handler;

use Exception;
use JetBrains\PhpStorm\NoReturn;
use Paneric\Migrations\Command\Config;
use Symfony\Component\Console\Output\OutputInterface;

class GenerateHandler
{
    use ConsoleOutputTrait;

    private array $config;

    public function __construct(Config $config)
    {
        $this->config = $config();
    }

    /**
     * @throws Exception
     */
    #[NoReturn]
    public function handle(string $path, string $suffix, OutputInterface $output): void
    {
        if (!array_key_exists($path, $this->config['folder_paths'])) {
            throw new Exception(sprintf(
                'Migrations folder path "%s" inconsistent with configuration.',
                $path
            ));
        }

        $namespace = $this->config['folder_paths'][$path];
        $migrationName = $this->prepareMigrationName($suffix);
        $params = [
            '{Namespace}' => $namespace,
            '{MigrationName}' => $migrationName,
        ];
        $template = $this->getTemplate();

        foreach ($params as $pattern => $replacement) {
            $template = preg_replace($pattern, $replacement, $template);
        }

        $this->saveMigrationFile($path, $migrationName, $template, $output);
    }

    private function prepareMigrationName(string $suffix): string
    {
        return sprintf(
            '%s%s%s',
            'Migration',
            date('YmdHis', strtotime('now')),
            ucfirst($suffix)
        );
    }

    private function getTemplate(): string
    {
        return
<<<END
<?php

declare(strict_types=1);

namespace Namespace;

use Exception;
use Paneric\Migrations\AbstractMigration;

final class MigrationName extends AbstractMigration
{
    private const SQL_PATH = '';

    public function getDescription(): string
    {
        return '';
    }

    /**
     * @throws Exception
     */
    public function execute(): void
    {
        \$this->executeQuery(file_get_contents(self::SQL_PATH));
    }
}

END;
    }

    /**
     * @throws Exception
     */
    private function saveMigrationFile(
        string $path,
        string $migrationName,
        string $template,
        OutputInterface $output
    ): void {
        if (!is_dir($path) && !mkdir($path, 0777, true)) {
            throw new Exception(sprintf(
                'Impossible to create migrations directory "%s".',
                $path
            ));
        }

        file_put_contents($path . $migrationName . '.php', $template);
        $this->notifyMigrationGeneration(
            $output,
            sprintf(
                '%s\\%s',
                $this->config['folder_paths'][$path],
                $migrationName
            )
        );
    }
}
