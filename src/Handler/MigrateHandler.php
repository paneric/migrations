<?php

declare(strict_types=1);

namespace Paneric\Migrations\Handler;

use Exception;
use Paneric\Migrations\Command\Config;
use Symfony\Component\Console\Formatter\OutputFormatterStyle;
use Symfony\Component\Console\Output\OutputInterface;

class MigrateHandler
{
    use ConsoleOutputTrait;
    
    private array $config;

    public function __construct(Config $config, readonly private MigrateService $migrateService)
    {
        $this->config = $config();
    }

    /**
     * @throws Exception
     */
    public function handle(string $migration, string $mode, OutputInterface $output): void
    {
        if ($migration === '') {
            $this->handleAll($mode, $output);
            return;
        }

        if (is_dir($migration)) {
            $this->handleFolder($migration, $mode, $output);
            return;
        }

        $this->handleFile($migration, $mode, $output);
    }
    
    /**
     * @throws Exception
     */
    private function handleAll(string $mode, OutputInterface $output): void
    {
        $folderPaths = array_keys($this->config['folder_paths']);
        foreach ($folderPaths as $folderPath) {
            if (is_dir($folderPath)) {
                $this->handleFolder($folderPath, $mode, $output);
            }
        }
    }

    /**
     * @throws Exception
     */
    private function handleFolder(string $folderPath, string $mode, OutputInterface $output): void
    {
        $filePaths = glob($folderPath . 'Migration*.php');

        foreach ($filePaths as $filePath) {
            $migrationData = $this->prepareMigrationData($folderPath, pathinfo($filePath, PATHINFO_FILENAME));
            $this->notifyMigrationExecution($output, $migrationData['namespace']);
            $this->migrateService->execute($migrationData);
        }
    }

    /**
     * @throws Exception
     */
    private function handleFile(string $migration, string $mode, OutputInterface $output): void
    {
        $migrationData = [];

        $folderPaths = array_keys($this->config['folder_paths']);
        foreach ($folderPaths as $folderPath) {
            $filePath = sprintf('%s%s.php', $folderPath, $migration);
            if (file_exists($filePath)) {
                $migrationData = $this->prepareMigrationData($folderPath, $migration);
                break;
            }
        }

        if (empty($migrationData)) {
            throw new Exception(sprintf(
                'Non-existing migration given "%s".',
                $migration
            ));
        }

        $this->notifyMigrationExecution($output, $migrationData['namespace']);
        $this->migrateService->execute($migrationData);
    }

    private function prepareMigrationData(string $folderPath, string $migration): array
    {
        $namespace = sprintf(
            '%s\\%s',
            $this->config['folder_paths'][$folderPath],
            $migration
        );

        return [
            'namespace' => $namespace,
            'folderPath' => $folderPath,
        ];
    }
}
