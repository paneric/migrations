<?php

declare(strict_types=1);

namespace Paneric\Migrations\Handler;

use Exception;
use JetBrains\PhpStorm\NoReturn;
use Paneric\Migrations\MigrationRepositoryInterface;

class MigrateService
{
    public function __construct(private readonly MigrationRepositoryInterface $migrationRepository)
    {
    }

    /**
     * @throws Exception
     */
    #[NoReturn]
    public function execute(array $migrationData): void
    {
        $migration = new ($migrationData['namespace'])($this->migrationRepository);
        $migration->execute();
    }
}
