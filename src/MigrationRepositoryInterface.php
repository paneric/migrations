<?php

declare(strict_types=1);

namespace Paneric\Migrations;

interface MigrationRepositoryInterface
{
    public function createTable(): void;
    
    public function findOneByRef(string $ref): mixed;

    /**
     * @throws Exception
     */
    public function execute(string $multiSql, array $migration);
}
