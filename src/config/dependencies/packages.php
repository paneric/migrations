<?php

declare(strict_types=1);

use Paneric\Migrations\Command\Config;
use Psr\Container\ContainerInterface;

return [
    Config::class => static function (ContainerInterface $container): Config {
        return new Config(
            $container->get('migrations'),
        );
    },
];
