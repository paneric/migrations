<?php

declare(strict_types=1);

return [
    'migrations' => [
        'folder_paths' => [
            'src/Migrations/' => 'Paneric\\Migrations\\Migrations',
        ],
    ],
];
